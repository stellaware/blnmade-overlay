# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="6"

DESCRIPTION="SL UDF"
HOMEPAGE="http://blnma.de/"

LICENSE="SL"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="dev-db/firebird"
RDEPEND="${DEPEND}"

inherit git-r3

EGIT_REPO_URI="git://blnma.de/sludf"

src_install()
{
	exeinto /usr/$(get_libdir)/firebird/UDF
	doexe ${PN}.so
}
