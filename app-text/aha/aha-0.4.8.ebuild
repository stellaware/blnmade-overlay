# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="Ansi HTML Adapter"
HOMEPAGE="https://github.com/theZiz/aha"
SRC_URI="https://github.com/theZiz/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="|| ( LGPL-2+ MPL-1.1 )"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_prepare() {
	sed -i \
		-e 's#MAN:=$(DESTDIR)$(PREFIX)/man/man1#MAN:=$(DESTDIR)$(PREFIX)/share/man/man1#' \
		-e 's#PREFIX=/usr/local#PREFIX=/usr#' \
		Makefile
}
