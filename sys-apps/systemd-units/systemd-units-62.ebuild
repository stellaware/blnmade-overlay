# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="systemd unit files not included in upstream packages"
HOMEPAGE="http://blnma.de"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ppc x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}"

inherit systemd

UNITS="
	cpupower.service
	kexec-load.service
	lircd.service
	nfs-exports.service
	nfs4-client.target
	nfs4-client.target
	nfs4-server.target
	nfs4-server.target
	o2cb@.service
	rpc.gssd.service
	rpc.idmapd.service
	rpc.mountd.service
	rpc.nfsd.service
	rpc.statd.service
	rpc.svcgssd.service
	radiusd.service
	sm-notify.service
	squid.service

	nagios.service
	npcd.service

	corosync.service
	rbd-map@.service

	addns-update.service
	addns-update.timer
"

SERVICED="
	ntpd.service.conf
	serial-getty@.service.conf
	snmpd.service.conf
"

src_install() {
	local unit
	for unit in $UNITS; do
		systemd_dounit "${FILESDIR}/${unit}"
	done

	for unit in $SERVICED; do
		systemd_install_serviced "${FILESDIR}/${unit}"
	done

	insinto /usr/lib/tmpfiles.d
	doins "${FILESDIR}/tmpfiles.d/uucp.conf"
	doins "${FILESDIR}/tmpfiles.d/nfsd.conf"
	if has_version dev-db/mysql || has_version dev-db/mariadb ; then
		doins "${FILESDIR}/tmpfiles.d/mysqld.conf"
	fi
	has_version app-misc/lirc && doins "${FILESDIR}/tmpfiles.d/lircd.conf"
	has_version net-dialup/freeradius && doins "${FILESDIR}/tmpfiles.d/radiusd.conf"

	if has_version net-fs/nfs-utils ; then
		insinto /usr/lib/modules-load.d
		doins "${FILESDIR}/modules-load.d/nfs.conf"
		doins "${FILESDIR}/modules-load.d/nfs-server.conf"
	fi

	has_version sys-apps/kexec-tools && systemd_enable_service kexec.target kexec-load.service
}
