# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils

DEBIAN_PV="${PV}"
DEBIAN_PR="4"
DEBIAN_P="${PN}-${DEBIAN_PV}"
DEBIAN_PF="${DEBIAN_P/-/_}-${DEBIAN_PR}"
DEBIAN_SRC="${DEBIAN_PF}.debian.tar.gz"
DESCRIPTION="T.38 fax modem"
HOMEPAGE="http://t38modem.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tgz
	mirror://debian/pool/main/t/${PN}/${DEBIAN_SRC}"

LICENSE="MPL-1.0"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

DEPEND="|| ( net-libs/opal[h323,wav] net-libs/opal[sip,wav] )"
RDEPEND="${DEPEND}"

src_prepare() {
	epatch "${WORKDIR}/debian/patches/simplify-makefile-new"
	epatch "${FILESDIR}/t38modem-opal3.10-patch"
	epatch "${WORKDIR}/debian/patches/spelling-error-fix"
	epatch "${WORKDIR}/debian/patches/opal-3.10.7.patch"
	epatch "${WORKDIR}/debian/patches/opal-3.10.9.patch"
	epatch "${FILESDIR}/t38modem-2.0.0-fix-baseclass-call.patch"
}

src_install() {
	dobin t38modem
	dodoc ReadMe.txt Changes.txt
	newdoc HylaFAX/config.ttyx HylaFAX.config.ttyx
}
