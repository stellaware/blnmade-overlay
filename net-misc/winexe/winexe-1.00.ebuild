# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
SRC_URI="mirror://sourceforge/winexe/${P}.tar.gz"

inherit eutils

DESCRIPTION="Winexe remotely executes commands on WindowsNT/2000/XP/2003 systems
from GNU/Linux"
HOMEPAGE="http://sourceforge.net/projects/winexe/"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

#DEPEND="dev-lang/python
#		dev-lang/swig"
DEPEND="sys-libs/tdb"
RDEPEND="${DEPEND}"

src_prepare() {
	epatch "${FILESDIR}/${P}-getopts.patch"
	epatch "${FILESDIR}/${P}-gnutls.patch"
	epatch "${FILESDIR}/${P}-pidl.patch"
}

src_configure() {
	cd "source4"
	./autogen.sh
	econf --enable-fhs
}

src_compile() {
	cd "source4"
	emake basics bin/winexe
}

src_install() {
	dobin "source4/bin/winexe"
}
