# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

LIBPE_COMMIT="7015c904d063e334103fccc5929d0cb17ce72c1e"

DESCRIPTION="The PE file analysis toolkit"
HOMEPAGE="https://github.com/merces/pev"
SRC_URI="https://github.com/merces/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/merces/libpe/archive/${LIBPE_COMMIT}.tar.gz -> libpe-${LIBPE_COMMIT}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

inherit eutils

DEPEND="dev-libs/libpcre
	dev-libs/openssl"
RDEPEND="${DEPEND}"

src_unpack() {
	default
	rmdir "${WORKDIR}/${P}/lib/libpe"
	mv "${WORKDIR}/libpe-${LIBPE_COMMIT}" "${WORKDIR}/${P}/lib/libpe"
	epatch "${FILESDIR}/${P}-destdir-fix.patch"
}
