# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="small and fast command line MP3 editor"
HOMEPAGE="http://www.puchalla-online.de/cutmp3.html"
SRC_URI="http://www.puchalla-online.de/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE=""

DEPEND="
	sys-libs/ncurses
	sys-libs/readline
"
RDEPEND=""

src_install()
{
	dobin cutmp3
	dodoc COPYING README README.timetable USAGE
	doman cutmp3.1
}
