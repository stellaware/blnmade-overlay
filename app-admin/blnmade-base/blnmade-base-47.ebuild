# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="Some basic settings and useful packages"
HOMEPAGE="httt://blnma.de/"
SRC_URI=""

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86 ppc"
IUSE="ads btrfs efi lvm ipmi salt +nano +vim systemd usb blnmade_modules_kernel blnmade_modules_vm blnmade_modules_container"

S="${WORKDIR}"

inherit systemd

#		!!sys-apps/debianutils[kernel_linux]
PDEPEND="
	app-portage/eix
	app-portage/euses
	app-portage/gentoolkit
	app-portage/portage-utils
	dev-vcs/git

	sys-apps/gptfdisk

	btrfs? ( sys-fs/btrfs-progs )
	efi? ( sys-boot/efibootmgr )
	lvm? ( sys-fs/lvm2 )
	ipmi? ( sys-apps/ipmiutil )

	systemd? (
		sys-apps/systemd
		>=sys-apps/systemd-units-35
	)

	usb? ( sys-apps/usbutils )
    vim? ( app-editors/vim )
	nano? ( app-editors/nano )

	app-misc/colordiff
	app-misc/tmux
	app-shells/bash-completion

	app-admin/sudo

	app-admin/logrotate
	sys-process/cronie
	sys-process/time

	sys-apps/less

	sys-apps/iproute2
	net-dns/bind-tools
	net-fs/nfs-utils

	ads? ( app-crypt/msktutil )

    blnmade_modules_kernel? (
		sys-kernel/gentoo-sources
		sys-kernel/linux-firmware
		sys-kernel/dracut
		app-arch/lzop
		sys-boot/syslinux
		sys-apps/kexec-tools
		sys-firmware/intel-microcode
	)

	!blnmade_modules_vm? (
		!blnmade_modules_container? (
			sys-power/cpupower
			sys-apps/smartmontools
			net-misc/ntp
		)
	)

	salt? (
		app-admin/salt
	)

	dev-util/strace
	sys-process/iotop
	sys-process/lsof
	net-analyzer/nettop
	net-analyzer/tcpdump
	sys-apps/dstat
	sys-apps/hdparm
	sys-block/fio
	sys-fs/ncdu
	sys-apps/pv
	app-text/aha

	sys-apps/cracklib-words
	sys-apps/dmidecode
	sys-apps/mlocate

	net-misc/adjtimex
"

SYSTEMD_UNITS="
	mnt-master.automount
	mnt-master.mount
	var-tmp-portage.mount

	defrag-btrfs@.service
	defrag-journal.timer
"

src_install() {
	if use vim ; then
		insinto /etc/vim
		doins "${FILESDIR}/vimrc.local"
	fi

	insinto /etc
	doins "${FILESDIR}/tmux.conf"

    insinto /etc/skel
    newins "${FILESDIR}/toprc" .toprc

	insinto /etc/bash/bashrc.d/
	doins "${FILESDIR}/bash_history.sh"
	doins "${FILESDIR}/tmux.sh"

	if use systemd ; then
		local unit
		for unit in $SYSTEMD_UNITS ; do
			systemd_dounit "${FILESDIR}/${unit}"
		done

		use blnmade_modules_container || systemd_enable_service multi-user.target mnt-master.automount

#		dosym /usr/bin/kernel-install /sbin/installkernel
	fi
}

pkg_postinst() {
	[ -f "/etc/portage/make.conf/99-celestilay" ] && mv "/etc/portage/make.conf/99-celestilay" "/etc/portage/make.conf/99-blnmade.conf"
}

pkg_config() {
	if ! [ -d "/etc/portage/make.conf" ]; then
		mv "/etc/portage/make.conf" "/etc/portage/make.conf.000"
		mkdir "/etc/portage/make.conf"
		mv "/etc/portage/make.conf.000" "/etc/portage/make.conf/00-make.conf"
	fi
	local cpus=$(($(nproc)))
	local jobs=$(((cpus - 1) / 4 + 1))
	((jobs < 2)) && jobs=2
	echo "MAKEOPTS=\"-j${jobs} -l${cpus}.0\"
EMERGE_DEFAULT_OPTS=\"\${EMERGE_DEFAULT_OPTS} --jobs ${jobs} --load-average ${cpus}.0\"" > "/etc/portage/make.conf/99-blnmade.conf"
}
