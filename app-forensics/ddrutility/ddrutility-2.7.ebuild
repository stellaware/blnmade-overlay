# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="A set of utilities to help with hard drive data rescue"
HOMEPAGE="http://ddrutility.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}
		app-forensics/sleuthkit
		sys-apps/util-linux
		sys-fs/ntfs3g
		sys-fs/ddrescue
	"
